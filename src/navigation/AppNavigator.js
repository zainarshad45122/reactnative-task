import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Home, AddNote, ViewNote } from "../screens";
import { View } from 'react-native';

const Stack = createStackNavigator();

export default AppNavigator = () => (
    <Stack.Navigator
        screenOptions = {{
            headerShown: null
        }}
    >     
        <Stack.Screen 
            name="Home" 
            component= {Home}
        /> 
         <Stack.Screen 
            name="AddNote" 
            component= {AddNote}
        />
          <Stack.Screen 
            name="ViewNote" 
            component= {ViewNote}
        />               
    </Stack.Navigator> 
)