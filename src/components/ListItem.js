import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
const COLORS = {primary: '#1f145c', white: '#fff'};

export default ListItem = ({todo, navigation}) => {
    return (
      <View style={styles.listItem}>
        <View style={{flex: 1}}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 15,
              color: COLORS.primary,
            }}>
            {todo?.title}
          </Text>
          <Text
            style={{
              fontSize: 15,
              color: COLORS.primary,
            }}>
            {todo?.description}
          </Text>
        </View>
        {!todo?.completed && (
          <TouchableOpacity  onPress={() => {
            navigation.navigate('ViewNote', {
              title: todo?.title,
              description: todo?.description,
            });
          }}>
            <View style={[styles.actionIcon, {backgroundColor: 'green'}]}>
              <Icon name="done" size={20} color="white" />
            </View>
          </TouchableOpacity>
        )}
      </View>
    );
  };

  const styles = StyleSheet.create({
    listItem: {
      padding: 20,
      backgroundColor: COLORS.white,
      flexDirection: 'row',
      elevation: 12,
      borderRadius: 7,
      marginVertical: 10,
    },
    actionIcon: {
      height: 25,
      width: 25,
      backgroundColor: COLORS.white,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'red',
      marginLeft: 5,
      borderRadius: 3,
    },
  });