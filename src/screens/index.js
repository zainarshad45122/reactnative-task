export { default as Login } from './Login';
export { default as SignUp } from './SignUp';
export { default as ForgetPassword } from './ForgetPassword';
export { default as AddNote } from './AddNote';
export { default as Home } from './Home';
export { default as ViewNote } from './ViewNote';