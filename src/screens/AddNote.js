import React from 'react';
import { Button, Input } from '../components';
import {AddNotes, SaveTodoToUserDevice} from '../services/app.js'
import {
    StyleSheet,
    View,
    Dimensions
  } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Alert } from 'react-native';
const {width, height} = Dimensions.get('window');

export default AddNote = ({navigation}) => {
  const [todos, setTodos] = React.useState([]);
  const [title, setTitle] = React.useState('');
  const [description, setDescription] = React.useState('');
  
  React.useEffect(() => {
    getTodosFromUserDevice();
  }, []);

    React.useEffect(() => {
        saveTodoToUserDevice(todos);
      }, [todos]);
    
      const addNote = () => {
        const newTodo = AddNotes(title,description);
        setTodos([...todos, newTodo]);
        setTitle('');
        setDescription('');
        Alert.alert('Note Added Successfully');
      };

      const getTodosFromUserDevice = async () => {
        const todos = await AsyncStorage.getItem('todos');
        console.log(todos);
        if (todos != null) {
            setTodos(JSON.parse(todos));
          }

      };
      const saveTodoToUserDevice = async todos1 => {
       SaveTodoToUserDevice(todos1);
      };


    return(
        <View style={styles.container}>
        <Input 
            placeholder= "Add Note Title"
            value={title}
            onChangeText={title => setTitle(title)}          
        />
        <Input 
             placeholder= "Add Note Description"
             value={description}
             onChangeText={description => setDescription(description)}            
        />
        <Button  
            buttonText= "Save Note"
            onPress={addNote}
        />
         <Button  
            buttonText= "Go back"
            onPress={() => navigation.goBack()}
        />
     
    </View>
    )
}

const styles = StyleSheet.create({
    container:{
        height,
        width,
        flex: 1,
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center'
    }
})