import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Text, View} from 'react-native';
import { Button, Input } from '../components';
const Stack = createStackNavigator();

export default ViewNote = ({ route, navigation }) => {

    const { title, description } = route.params;

    return(
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Note Detail</Text>
        <Text style={{
              fontWeight: 'bold',
              fontSize: 15,
        }}>Title: {title}</Text>
        <Text>Description: </Text>
        <Text>{description}</Text>
        <Button  
            buttonText= "Go back"
            onPress={() => navigation.goBack()}
        />
      </View>
    )
}