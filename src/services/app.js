import { Alert } from 'react-native';
  import AsyncStorage from '@react-native-async-storage/async-storage';

export const AddNotes = (title,description) => {
    if (title == '' || description == '') {
      Alert.alert('Error', 'Please enter all fields');
    } else {
      const newTodo = {
        id: Math.random(),
        title: title,
        description : description,
        completed: false,
      };
     return newTodo;
    }
  };

export  const SaveTodoToUserDevice = async todos1 => {
  try {
    const stringifyTodos = JSON.stringify(todos1);
    await AsyncStorage.setItem('todos', stringifyTodos);
  } catch (error) {
    console.log(error);
  }
};

